import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';
import { BaseLibraryComponent } from './base-library.component';
import { BaseLibraryService } from './base-library.service';
import { SpinnerService } from './spinner.service';



@NgModule({
  declarations: [
    BaseLibraryComponent
  ],
  imports: [
  ],
  exports: [
    BaseLibraryComponent
  ]
})
export class BaseLibraryModule {
	static forRoot(standalone: boolean): ModuleWithProviders<BaseLibraryModule> {
    if(standalone){ // <<<<<--- Si queremos correr standalone o no el Microfrontend o Shell que utilice el Servicio
      return {
        ngModule: BaseLibraryModule, // <<<<<--- Modulo expuesto
        providers: [BaseLibraryService, SpinnerService], // <<<<<--- Quien corra standalone necesitara su propia instancia
      };
    }
		return {
			ngModule: BaseLibraryModule, // <<<<<--- Modulo expuesto
      providers: [], // <<<<<--- Evitamos multiples instancias al correr en conjunto
		};
	}
}