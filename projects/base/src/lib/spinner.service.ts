import { HostListener, Injectable } from '@angular/core';
import { AnimationPlayer } from '@angular/animations';

@Injectable()
export class SpinnerService {
  private spinnerElement: any;
  public player: AnimationPlayer;
  public expandedSideNav: boolean = false;
  public isMobile: boolean;
  scrWidth: number;

  constructor() {
    // console.log('Se instancia el Spinner')
    this.scrWidth = window.innerWidth;
    this.changeIsMobile();
  }

  init(element) {
    this.spinnerElement = element;
    this.hide();
    // console.log(this.spinnerElement);
  }

  public show() {
    this.scrWidth = window.innerWidth;
    this.changeIsMobile();

    /**
     * Si es mobile el spinner quedara centrado en el total de la pantalla
     * Si no es mobile y el sidenav esta cerrado el spinner quedara centrado en la pantalla
     * Si no es mobile y el sidenav esta abierto el spinner quedara centrado al contenedor del mf
     */
    if( this.expandedSideNav && !this.isMobile ){
      this.spinnerElement.style.width = 'calc(100% - 250px)';
      this.spinnerElement.style.padding = '0px 30px 0px 220px';
    } else {
      this.spinnerElement.style.width = '100%';
      this.spinnerElement.style.padding = '0px';
    }
    this.spinnerElement.style.visibility = 'inherit';
  }

  public hide() {
    this.spinnerElement.style.visibility = 'hidden';
  }

  public changeExpandedSidenav(state?: boolean){
    if( state != undefined && state != null ){
      this.expandedSideNav = state;
    } else {
      this.expandedSideNav = !this.expandedSideNav;
    }
  }

  public changeIsMobile(){
    if( this.scrWidth  < 768 ){
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }
}
