import { EventEmitter, Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { of, Observable, Subject } from 'rxjs';

@Injectable()
export class BaseLibraryService {
  @Output() changeTenant: EventEmitter<any> = new EventEmitter();
  // @Output() getRoutes: EventEmitter<any> = new EventEmitter();
  
  constructor(private router: Router) {
    // console.log('se instancia la libreria compartida.');
  }

  emitTenantChange() {
    this.changeTenant.emit();
  }  

  checkPermiso(codigoPermiso: string): Observable<any> {
    let founded: boolean = false;

    this.getSessionStorageItemValue('permisosList').subscribe((res) => {
      founded = res.includes(codigoPermiso) ? true : false;
    });
    return of(founded);
  }

  clearAtLogout() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('permisosList');
    localStorage.removeItem('menuItems');
  }

  setSessionStorageItem(key: string, value: any) {
    sessionStorage.setItem(key, value);
    // console.log('Insertado: ' + key + ' en Library, ' + 'value: ' + value)
  }

  getSessionStorageItemValue(key: string): Observable<any> {
    const value = sessionStorage.getItem(key);
    if (value && value !== '') {
      return of(value);
    } else {
      return of('');
    }
  }

  // comparte comportamiento cuando se cierra la session
  public inactiveFlag = new Subject<boolean>();
  inactiveFlag$ = this.inactiveFlag.asObservable();

  inactiveStatusFlag(flag : boolean){
    this.inactiveFlag.next(flag);
  }

  public getMenuItems() {
		const menuItems = localStorage.getItem('menuItems');
		if (menuItems) {
			return JSON.parse(menuItems);
		}
		else {
			return [];
		}
	}

  public getActualMenu(): Observable<any>{    
    const menuItems = this.getMenuItems();
    const currentRouteUrl = this.router.url.split(/[?#]/)[0];
    const actualMenu = menuItems.filter(item => item.page == currentRouteUrl);

		if (actualMenu) {
			return of(actualMenu[0]);
		}
		else {
			return of('');
		}
	}

  // public missionAnnouncedSource = new Subject<string>();
  // missionAnnounced$ = this.missionAnnouncedSource.asObservable();

  // announceMission(mission: string) {
  //   this.missionAnnouncedSource.next(mission);
  // }

  // public loggedInSubject = new Subject<boolean>();
  // loggedInObs$ = this.loggedInSubject.asObservable();

  // public loggedIn: boolean = false;

  // actualizaLoggedInState(state: boolean) {
  //   this.loggedInSubject.next(state);
  // }
  
}
